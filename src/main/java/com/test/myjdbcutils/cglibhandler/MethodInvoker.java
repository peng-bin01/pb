package com.test.myjdbcutils.cglibhandler;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author pb
 */
public class MethodInvoker {
    private MethodProxy methodProxy;
    private Object o;
    private Object[] objects;
    private Method method;

    public MethodInvoker(Object o, Object[] objects, Method method) {
        this.o = o;
        this.objects = objects;
        this.method = method;
    }

//    public Object invoke() {
//        try {
//            return this.methodProxy.invokeSuper(this.o, objects);
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
//        return null;
//    }

    public Object invoke() {
        try {
            return this.method.invoke(this.o,this.objects);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }
}