package com.test.myjdbcutils.cglibhandler;

import com.test.myjdbcutils.connectonutils.ConnectionUtils;
import com.test.myjdbcutils.entity.MethodInfo;
import com.test.myjdbcutils.utils.SqlHandler;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author pb
 */
public class MyMapperInterceptor implements MethodInterceptor {
    private Connection connection;
    private Map<String, MethodInfo> sqlS;
    private Logger logger = LogManager.getLogger(this.getClass());

    public void setSqlS(Map<String, MethodInfo> sqlS) throws PropertyVetoException, SQLException, IOException {
        this.sqlS = sqlS;
        this.connection = ConnectionUtils.dateSources().getConnection();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        if (!sqlS.containsKey(method.getName())) {
            return methodProxy.invokeSuper(o, objects);
        }
        MethodInfo methodInfo = this.sqlS.get(method.getName());
        String sql = methodInfo.getSql();
        MethodInfo argsInfo = SqlHandler.sqlPackage(sql, method, objects);
        assert argsInfo != null;
        sql = argsInfo.getSql();
        Object[] argsList = argsInfo.getArgsList();
        logger.info("sql:{}",sql);
        Object o1 = Arrays.stream(argsList).reduce((arg1, arg2) -> String.valueOf(arg1).concat(",").concat(String.valueOf(arg2))).orElse("");
        logger.info("参数:{}", o1);
        if (sql.startsWith("select")) {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < argsList.length; i++) {
                if (argsList[i] instanceof Integer) {
                    preparedStatement.setInt(i + 1, (Integer) argsList[i]);
                } else if (argsList[i] instanceof String) {
                    preparedStatement.setString(i + 1, (String) argsList[i]);
                } else if (argsList[i] instanceof Double) {
                    preparedStatement.setDouble(i + 1, (Double) argsList[i]);
                }else{
                    preparedStatement.setString(i + 1, null);
                }
            }
            resultSet = preparedStatement.executeQuery();
            Class genericReturnType = methodInfo.getReturnType();
            boolean isList = false;
            Type genericReturnType1 = method.getGenericReturnType();
            if (genericReturnType1 instanceof ParameterizedType) {
                isList = true;
            }

            List<?> returnS = SqlHandler.dataPackage(resultSet, genericReturnType);
            if (isList) {
                return returnS;
            } else if (returnS.size() != 0) {
                return returnS.get(0);
            }
            return null;
        } else {
            PreparedStatement preparedStatement = null;
            preparedStatement = connection.prepareStatement(sql);
            return preparedStatement.executeUpdate();
        }
    }
}
