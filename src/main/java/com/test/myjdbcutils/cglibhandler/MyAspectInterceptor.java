package com.test.myjdbcutils.cglibhandler;

import com.test.myjdbcutils.entity.BeanContainer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author pb
 */
public class MyAspectInterceptor implements MethodInterceptor {
    private Map<Method, Method> methodMethodMap;

    public void setProxy(Map<Method, Method> methodMethodMap) {
        this.methodMethodMap = methodMethodMap;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        Method method1 = this.methodMethodMap.get(method);
        if (method1 != null) {
            MethodInvoker methodInvoker = new MethodInvoker(BeanContainer.getInstance().getOriginalBean(o.getClass().getSuperclass()), objects, method);
            Class<?> declaringClass = method1.getDeclaringClass();
            Object bean = BeanContainer.getInstance().getBean(declaringClass);
            return method1.invoke(bean, methodInvoker);
        } else {
            //被代理的类容器中原类
            Object originalBean = BeanContainer.getInstance().getOriginalBean(o.getClass().getSuperclass());
            Method method2 = originalBean.getClass().getMethod(method.getName(), method.getParameterTypes());
            return method2.invoke(originalBean,objects);
        }
    }

}
