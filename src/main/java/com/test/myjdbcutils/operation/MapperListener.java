package com.test.myjdbcutils.operation;

import com.test.myjdbcutils.cglibhandler.MyMapperInterceptor;
import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.entity.BeanInfo;
import com.test.myjdbcutils.entity.MethodInfo;
import com.test.myjdbcutils.myannotations.MyEntity;
import com.test.myjdbcutils.myannotations.MyMapper;
import com.test.myjdbcutils.utils.SqlHandler;
import net.sf.cglib.proxy.Enhancer;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author pb
 */
@MyEntity
public class MapperListener implements Listener {
    private List<BeanInfo> beanInfos;
    private MyMapperInterceptor myMapperInterceptor = new MyMapperInterceptor();

    @Override
    public void handler(Map<String, BeanInfo> beanInfoMap) throws Exception {
        for (BeanInfo beanInfo : this.beanInfos) {
            Object o = Enhancer.create(beanInfo.getaClass(), this.myMapperInterceptor);
            beanInfo.setBeAspectFlag(true,o);
        }
    }

    @Override
    public void preHandler(Map<String, BeanInfo> beanInfoMap) throws PropertyVetoException, SQLException, IOException {
        this.beanInfos = BeanContainer.getInstance().getBeanInfoListByAnnotation(MyMapper.class);
        List<Class> collect = beanInfos.stream().map(BeanInfo::getaClass).collect(Collectors.toList());
        Map<String, MethodInfo> sqlS= SqlHandler.getSql(collect);
        Map<String, MethodInfo> baseSql = SqlHandler.getBaseSql(collect);
        sqlS.putAll(baseSql);
        this.myMapperInterceptor.setSqlS(sqlS);
    }

    @Override
    public void afterHandler(Map<String, BeanInfo> beanInfoMap) throws Exception {

    }

    @Override
    public int order() {
        return 2;
    }
}
