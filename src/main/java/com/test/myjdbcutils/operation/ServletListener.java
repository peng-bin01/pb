package com.test.myjdbcutils.operation;

import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.entity.BeanInfo;
import com.test.myjdbcutils.myannotations.MyController;
import com.test.myjdbcutils.myannotations.MyEntity;
import com.test.myjdbcutils.myannotations.MyRequestMapping;
import com.test.myjdbcutils.utils.ControllerManagerServlet;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyVetoException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pb
 */
@MyEntity
public class ServletListener implements Listener {
    private Map<String, Method> methodStringMap = new HashMap<>();

    private Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public void handler(Map<String, BeanInfo> beanInfoMap) throws Exception {
    }

    @Override
    public void preHandler(Map<String, BeanInfo> beanInfoMap) throws PropertyVetoException, SQLException {
        List<BeanInfo> beanInfos = BeanContainer.getInstance().getBeanInfoListByAnnotation(MyController.class);
        for (BeanInfo beanInfo : beanInfos) {
            Map<Method, List<Annotation>> methodAnnotations = beanInfo.getMethodAnnotations();
            for (Map.Entry<Method, List<Annotation>> next : methodAnnotations.entrySet()) {
                Method key = next.getKey();
                List<Annotation> value = next.getValue();
                for (Annotation annotation : value) {
                    if (annotation.annotationType().equals(MyRequestMapping.class)) {
                        logger.info("路径映射方法：{},{}",((MyRequestMapping) annotation).value(),key);
                        this.methodStringMap.put(((MyRequestMapping) annotation).value(), key);
                    }
                }
            }
        }
    }

    @Override
    public void afterHandler(Map<String, BeanInfo> beanInfoMap) throws Exception {
        Map<String, String> configMap = BeanContainer.getInstance().getConfigMap();
        String port = configMap.get("tomcat.port");
        String dir = configMap.get("tomcat.dir");
        logger.info("tomcat启动端口：{}",port);
        logger.info("tomcat工作目录：{}",dir);
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(Integer.parseInt(port));
        //相对路径 当前项目地址根目录 user.dir
        tomcat.setBaseDir(dir);
        String contextPath = "";
        StandardContext context = new StandardContext();
        context.setPath(contextPath);
//        context.setRequestCharacterEncoding("UTF-8");
        context.setResponseCharacterEncoding("UTF-8");
        context.addLifecycleListener(new Tomcat.FixContextListener());
        tomcat.getHost().addChild(context);
        ControllerManagerServlet servlet = new ControllerManagerServlet();
        servlet.setMethodPath(this.methodStringMap);
        tomcat.addServlet(contextPath, "controllerManager", servlet);
        context.addServletMappingDecoded("/*", "controllerManager");
        logger.info("注入请求转发servlet：{}",servlet.getClass());
        tomcat.start();
        logger.info("项目启动完毕");
        BeanContainer.clear();
        tomcat.getServer().await();
    }

    @Override
    public int order() {
        return 100;
    }
}
