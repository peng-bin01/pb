package com.test.myjdbcutils.operation;

import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.entity.BeanInfo;
import com.test.myjdbcutils.myannotations.MyBean;
import com.test.myjdbcutils.myannotations.MyConfiguration;
import com.test.myjdbcutils.myannotations.MyEntity;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pb
 */
@MyEntity
public class ConfigListener implements Listener {
    private List<BeanInfo> beanInfoListByAnnotation;
    private Map<Method, Class[]> methodArgsMap = new HashMap<>();
    private Map<Class, Method> methodReturnMap = new HashMap<>();
    private Map<Method, Boolean> doCreate = new HashMap<>();

    @Override
    public void handler(Map<String, BeanInfo> beanInfoMap) throws Exception {
        for (Map.Entry<Method, Class[]> methodEntry : this.methodArgsMap.entrySet()) {
            this.createBean(methodEntry.getKey(), methodEntry.getValue(), beanInfoMap);
        }
    }

    @Override
    public void preHandler(Map<String, BeanInfo> beanInfoMap) throws PropertyVetoException, SQLException, IOException, InvocationTargetException, IllegalAccessException {
        this.beanInfoListByAnnotation = BeanContainer.getInstance().getBeanInfoListByAnnotation(MyConfiguration.class);
        for (BeanInfo beanInfo : this.beanInfoListByAnnotation) {
            Map<Method, List<Annotation>> methodAnnotations = beanInfo.getMethodAnnotations();
            for (Map.Entry<Method, List<Annotation>> methodListEntry : methodAnnotations.entrySet()) {
                List<Annotation> value = methodListEntry.getValue();
                for (Annotation annotation : value) {
                    if (annotation.annotationType().equals(MyBean.class)) {
                        Method key = methodListEntry.getKey();
                        Class<?>[] parameterTypes = key.getParameterTypes();
                        this.methodArgsMap.put(key, parameterTypes);
                        this.methodReturnMap.put(key.getReturnType(), key);
                        this.doCreate.put(key, false);
                    }
                }
            }
        }
    }

    @Override
    public void afterHandler(Map<String, BeanInfo> beanInfoMap) throws Exception {

    }

    @Override
    public int order() {
        return 1;
    }

    private void createBean(Method key, Class[] parameterTypes, Map<String, BeanInfo> beanInfoMap) throws InvocationTargetException, IllegalAccessException {
        //方法是否执行过
        if (this.doCreate.get(key)) {
            return;
        }
        this.doCreate.put(key, true);
        Object[] args = new Object[parameterTypes.length];
        if (BeanContainer.getInstance().getClassList().contains(key.getReturnType())) {
            return;
        }
        for (int i = 0; i < parameterTypes.length; i++) {
            Object bean = BeanContainer.getInstance().getBean(parameterTypes[i]);
            if (bean == null) {
                Method method = this.methodReturnMap.get(parameterTypes[i]);
                //需要的类是否不存在bean容器和待生成的bean中
                if (method != null) {
                    this.createBean(method, this.methodArgsMap.get(method), beanInfoMap);
                }
            }
            bean = BeanContainer.getInstance().getBean(parameterTypes[i]);
            args[i] = bean;
        }
        Object invoke = key.invoke(beanInfoMap.get(key.getDeclaringClass().getTypeName()).getBean(), args);
        BeanContainer.getInstance().getClassList().add(invoke.getClass());
        BeanContainer.getInstance().getBeanList().add(invoke);
        BeanInfo beanInfo1 = new BeanInfo();
        beanInfo1.setaClass(invoke.getClass());
        beanInfo1.setBean(invoke);
        beanInfo1.setBeanName(invoke.getClass().getTypeName());
        beanInfo1.setFieldAnnotations(new HashMap<>(0));
        beanInfo1.setMethodAnnotations(new HashMap<>(0));
        BeanContainer.getInstance().getBeanInfos().add(beanInfo1);
        beanInfoMap.put(invoke.getClass().getTypeName(), beanInfo1);
    }
}
