package com.test.myjdbcutils.operation;

import com.test.myjdbcutils.cglibhandler.MyAspectInterceptor;
import com.test.myjdbcutils.entity.AspectInfo;
import com.test.myjdbcutils.entity.BeanInfo;
import com.test.myjdbcutils.myannotations.MyAopAnnotation;
import com.test.myjdbcutils.myannotations.MyAopClassAnnotation;
import com.test.myjdbcutils.myannotations.MyEntity;
import net.sf.cglib.proxy.Enhancer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author pb
 */
@MyEntity
public class AspectListener implements Listener {
    //注解类和切面的对应
    private Map<Class, Method> methodClassMap = new HashMap<>();
    //被切的类信息
    private Map<Class,AspectInfo> aspectInfo = new HashMap<>();

    @Override
    public void handler(Map<String, BeanInfo> beanInfoMap) throws Exception {
        for (Map.Entry<String, BeanInfo> next : beanInfoMap.entrySet()) {
            BeanInfo value = next.getValue();
            if (value.getaClass().getAnnotation(MyAopClassAnnotation.class) != null) {
                Map.Entry<Method, List<Annotation>> methodByAnnotation = value.getMethodByAnnotation(MyAopAnnotation.class);
                for (Annotation annotation : methodByAnnotation.getValue()) {
                    if (annotation.annotationType().equals(MyAopAnnotation.class)) {
                        MyAopAnnotation myAopAnnotation = (MyAopAnnotation) annotation;
                        Class aClass = myAopAnnotation.classValue();
                        methodClassMap.put(aClass, methodByAnnotation.getKey());
                    }
                }
            }
        }
        this.doAspect(methodClassMap, beanInfoMap);
    }

    @Override
    public void preHandler(Map<String, BeanInfo> beanInfoMap) {

    }

    @Override
    public void afterHandler(Map<String, BeanInfo> beanInfoMap) {

    }

    @Override
    public int order() {
        return 10;
    }

    private void doAspect(Map<Class, Method> methodClassMap, Map<String, BeanInfo> beanInfoMap) throws NoSuchMethodException {
        List<Class> classes = new ArrayList<>(methodClassMap.keySet());
        for (Map.Entry<String, BeanInfo> next : beanInfoMap.entrySet()) {
            Map<Method, List<Annotation>> methodAnnotations = next.getValue().getMethodAnnotations();
            Map<Method,Method> methodMethodMap = new HashMap<>();
            for (Map.Entry<Method, List<Annotation>> methodAnnotation : methodAnnotations.entrySet()) {
                List<Annotation> value = methodAnnotation.getValue();
                for (Annotation annotation : value) {
                    if (classes.contains(annotation.annotationType())) {
                        //代理方法
                        Method method = this.methodClassMap.get(annotation.annotationType());
                        //被代理方法
                        Method method1 = methodAnnotation.getKey();
                        methodMethodMap.put(method1,method);
                    }
                }
            }
            if(methodMethodMap.size() != 0){

                if(this.aspectInfo.containsKey(next.getValue().getaClass())){
                    AspectInfo aspectInfo = this.aspectInfo.get(next.getValue().getaClass());
                    aspectInfo.getMethodMethodMap().putAll(methodMethodMap);
                }else{
                    AspectInfo aspectInfo = new AspectInfo();
                    aspectInfo.setBeAspectClass(next.getValue().getaClass());
                    aspectInfo.setMethodMethodMap(methodMethodMap);
                    this.aspectInfo.put(next.getValue().getaClass(),aspectInfo);
                }
            }
        }
        this.aspectBeanCreate(beanInfoMap);
    }

    private void aspectBeanCreate(Map<String, BeanInfo> beanInfoMap) {
        Collection<AspectInfo> values = this.aspectInfo.values();
        for (AspectInfo value : values) {
            Class beAspectClass = value.getBeAspectClass();
            Map<Method, Method> methodMethodMap = value.getMethodMethodMap();
            MyAspectInterceptor callback = new MyAspectInterceptor();
            callback.setProxy(methodMethodMap);
            Object o = Enhancer.create(beAspectClass, callback);
            for (BeanInfo beanInfo : beanInfoMap.values()) {
                if (beanInfo.getaClass().equals(beAspectClass)) {
                    beanInfo.setBeAspectFlag(true,o);
                }
            }
        }
    }
}
