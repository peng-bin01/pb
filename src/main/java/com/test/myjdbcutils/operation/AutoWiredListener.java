package com.test.myjdbcutils.operation;

import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.entity.BeanInfo;
import com.test.myjdbcutils.myannotations.MyAutoWired;
import com.test.myjdbcutils.myannotations.MyEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author pb
 */
@MyEntity
public class AutoWiredListener implements Listener {
    private BeanContainer beanContainer = BeanContainer.getInstance();
    private Logger logger = LogManager.getLogger(this.getClass());

    /**
     * 生成bean并且自动注入
     *
     * @param beanInfoMap bean信息
     * @throws Exception 异常
     */
    @Override
    public void handler(Map<String, BeanInfo> beanInfoMap) throws Exception {
        Iterator<Map.Entry<String, BeanInfo>> iterator = beanInfoMap.entrySet().iterator();
        ArrayList<Object> objects = new ArrayList<>();
        while (iterator.hasNext()) {
            Map.Entry<String, BeanInfo> next = iterator.next();
            Class aClass = next.getValue().getaClass();
            Constructor constructor = aClass.getConstructor();
            Object o = constructor.newInstance();
            objects.add(o);
            next.getValue().setBean(o);
        }
        this.beanContainer.setBeanList(objects);

    }

    /**
     * 增加bean容器生成别名体系
     *
     * @param beanInfoMap bean信息
     */
    @Override
    public void preHandler(Map<String, BeanInfo> beanInfoMap) {
        Iterator<Map.Entry<String, BeanInfo>> iterator = beanInfoMap.entrySet().iterator();
        Map<String, Class> map = new HashMap<>();
        while (iterator.hasNext()) {
            Map.Entry<String, BeanInfo> next = iterator.next();
            BeanInfo value = next.getValue();
            if (!StringUtils.isEmpty(value.getBeanName())) {
                map.put(value.getBeanName(), value.getaClass());
            }
        }
        beanContainer.setAlisMap(map);
    }

    @Override
    public void afterHandler(Map<String, BeanInfo> beanInfoMap) throws Exception {
        for (Map.Entry<String, BeanInfo> next : beanInfoMap.entrySet()) {
            Class aClass = next.getValue().getaClass();
            Map<String, List<Annotation>> fieldAnnotations = next.getValue().getFieldAnnotations();
            if (fieldAnnotations != null) {
                for (Map.Entry<String, List<Annotation>> field : fieldAnnotations.entrySet()) {
                    List<Annotation> value = field.getValue();
                    String fieldName = field.getKey();
                    for (Annotation annotation : value) {
                        if (annotation.annotationType().equals(MyAutoWired.class)) {
                            MyAutoWired myAutoWired = (MyAutoWired) annotation;
                            Map<String, Class> alisMap = beanContainer.getAlisMap();
                            Field declaredField = aClass.getDeclaredField(fieldName);
                            Class entityClass = null;
                            BeanInfo beanInfo = null;

                            if (!StringUtils.isEmpty(myAutoWired.value())) {
                                entityClass = alisMap.get(myAutoWired.value());
                                if (entityClass == null) {
                                    throw new Exception("无对应名称实体：" + myAutoWired.value());
                                }
                                beanInfo = beanInfoMap.get(entityClass.getTypeName());
                            } else {
                                beanInfo = beanInfoMap.get(declaredField.getType().getTypeName());
                            }
                            declaredField.setAccessible(true);
                            Object bean = beanInfo.getBean();
                            if (beanInfo.isBeAspectFlag()) {
                                bean = beanInfo.getAspectBean();
                            }
                            declaredField.set(next.getValue().getBean(), bean);

                        }
                    }
                }
            }
            logger.info("生成bean：{}",aClass.getTypeName());
        }
    }

    @Override
    public int order() {
        return -1;
    }
}
