package com.test.myjdbcutils.operation;

import com.test.myjdbcutils.entity.BeanInfo;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author pb
 */
public interface Listener {
    /**
     * 处理各种本框架集成的annotation
     *
     * @param beanInfoMap bean信息
     */
    public void handler(Map<String, BeanInfo> beanInfoMap) throws Exception;

    /**
     * 具体框架生成bean之前操作
     *
     * @param beanInfoMap bean信息
     */
    public void preHandler(Map<String, BeanInfo> beanInfoMap) throws PropertyVetoException, SQLException, IOException, InvocationTargetException, IllegalAccessException;

    /**
     * 具体框架生成bean之后操作
     *
     * @param beanInfoMap bean信息
     */
    public void afterHandler(Map<String, BeanInfo> beanInfoMap) throws Exception;

    /**
     * 具体框架生成bean之后操作
     * @return 排序号越小越在前面
     */
    public int order();
}
