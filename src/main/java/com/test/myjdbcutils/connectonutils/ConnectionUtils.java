package com.test.myjdbcutils.connectonutils;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.test.myjdbcutils.entity.BeanContainer;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author pb
 */
public class ConnectionUtils {
    private static Map<String,String> configMap = BeanContainer.getInstance().getConfigMap();
    public static Connection getConnection() throws ClassNotFoundException, SQLException, IOException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = configMap.get("pb.url");
        String username = configMap.get("pb.username");
        String password = configMap.get("pb.password");
        return DriverManager.getConnection(url,username,password);
    }
    public static DataSource dateSources() throws PropertyVetoException, IOException {
        ComboPooledDataSource ds = new ComboPooledDataSource();
        String url = configMap.get("pb.url");
        String username = configMap.get("pb.username");
        String password = configMap.get("pb.password");

        ds.setDriverClass("com.mysql.jdbc.Driver");

        ds.setJdbcUrl(url);

        ds.setUser(username);

        ds.setPassword(password);

        ds.setInitialPoolSize(50);

        ds.setMaxPoolSize(100);

        ds.setMaxIdleTime(10000);

        return ds;
    }
}
