package com.test.myjdbcutils.test;

import java.util.concurrent.ThreadFactory;

/**
 * @author pb
 */
public class MyThreadFactory implements ThreadFactory {
    private int number;

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setName("bagayalu" + number);
        number++;
        return thread;
    }
}
