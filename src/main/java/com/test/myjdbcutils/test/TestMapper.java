package com.test.myjdbcutils.test;

import com.mchange.v2.beans.swing.TestBean;
import com.test.myjdbcutils.basemapper.BaseMapper;
import com.test.myjdbcutils.myannotations.MyJDBCAnnotation;
import com.test.myjdbcutils.myannotations.MyMapper;
import com.test.myjdbcutils.myannotations.MyMapperParam;

/**
 * @author pb
 */
@MyMapper
public class TestMapper extends BaseMapper<TestEntity> {
    public void test(){
        System.out.println("测试方法");
    }
    @MyJDBCAnnotation(sql = "select * from test1 where id = #{0} AND label = #{1}")
    public TestEntity test1(int id, String label){
        return null;
    }

    @MyJDBCAnnotation(sql = "insert into api_security (path,authorities) VALUES ('/test','p3,p4')")
    public Integer test2(){
        return null;
    }

}
