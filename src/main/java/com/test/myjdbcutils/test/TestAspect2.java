package com.test.myjdbcutils.test;

import com.test.myjdbcutils.cglibhandler.MethodInvoker;
import com.test.myjdbcutils.myannotations.*;
import com.test.myjdbcutils.springutils.MyFactoryBean;

/**
 * @author pb
 */
@MyAopClassAnnotation
@MyEntity
public class TestAspect2 {
    @MyAutoWired
    private TestEntity2 testEntity2;
    @MyAutoWired
    private MyFactoryBean myFactoryBean;

    @MyAopAnnotation(value = "AopTestAnnotation2", classValue = AopTestAnnotation2.class)
    public Object testAop2(MethodInvoker methodInvoker) {
        System.out.println("切面执行中");
        Object invoke = methodInvoker.invoke();
        System.out.println("切面执行完毕");
        return invoke;
    }
}
