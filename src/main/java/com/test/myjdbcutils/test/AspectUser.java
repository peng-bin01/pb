package com.test.myjdbcutils.test;

import com.test.myjdbcutils.myannotations.AopTestAnnotation;
import com.test.myjdbcutils.myannotations.MyAutoWired;
import com.test.myjdbcutils.myannotations.MyEntity;

import java.util.List;

/**
 * @author pb
 */
@MyEntity
public class AspectUser {
    @MyAutoWired
    private TestEntity2 testEntity2;
    @MyAutoWired
    private TestMapper testMapper;

    @AopTestAnnotation
    public void test(){
        List<TestEntity> testEntities = this.testMapper.selectAll();
        System.out.println(testEntities);
    }

    public void test2(){
        System.out.println(this.testEntity2.test("a"));
    }
}
