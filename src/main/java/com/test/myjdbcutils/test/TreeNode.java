package com.test.myjdbcutils.test;

import lombok.Data;

/**
 * @email bin.peng01@hand-china.com
 * @author 28378
 * @date 2021/10/4 16:51
 */
@Data
public class TreeNode {
    private TreeNode left;
    private TreeNode right;
    private String data;
}
