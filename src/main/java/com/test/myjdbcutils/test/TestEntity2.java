package com.test.myjdbcutils.test;

import com.test.myjdbcutils.myannotations.AopTestAnnotation;
import com.test.myjdbcutils.myannotations.AopTestAnnotation2;
import com.test.myjdbcutils.myannotations.MyAutoWired;
import com.test.myjdbcutils.myannotations.MyEntity;

/**
 * @author pb
 */
@MyEntity("testEntity2")
public class TestEntity2 {
    @MyAutoWired("1")
    private TestEntity testEntity;
    @AopTestAnnotation
    public String test(){
        System.out.println("具体代码执行中");
        this.testEntity.setId(1);
        return "ssss";
    }

    @AopTestAnnotation2
    public String test(String s){
        return s;
    }

    public void setTestEntity(TestEntity testEntity) {
        this.testEntity = testEntity;
    }

    public TestEntity getTestEntity() {
        return testEntity;
    }
}
