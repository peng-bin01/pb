package com.test.myjdbcutils.test;

import com.test.myjdbcutils.cglibhandler.MethodInvoker;
import com.test.myjdbcutils.myannotations.*;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author pb
 */
@MyAopClassAnnotation
@MyEntity
public class TestAspect {
    @MyAutoWired
    private TestEntity2 testEntity2;

    @MyAopAnnotation(value = "AopTestAnnotation", classValue = AopTestAnnotation.class)
    public Object testAop(MethodInvoker methodInvoker) {
        System.out.println("切面执行中");
        Object invoke = methodInvoker.invoke();
        System.out.println("切面执行完毕");
        return invoke;
    }


    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        char[] chars = String.valueOf(x).toCharArray();
        int j = chars.length - 1;
        for (int i = 0; i <= j; i++, j--) {
            if (chars[i] != chars[j]) {
                return false;
            }
            if (i == j || i == j - 1) {
                return true;
            }
        }
        return false;
    }

    public String intToRoman(int num) {
        int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] reps = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            int value = values[i];
            while (num >= value) {
                num = num - value;
                result.append(reps[i]);
            }
        }
        return result.toString();
    }

    public String longestCommonPrefix(String[] strs) {
        int min = 300;
        ArrayList<char[]> charsL = new ArrayList<>(strs.length);
        StringBuilder result = new StringBuilder();
        for (String str : strs) {
            char[] chars = str.toCharArray();
            if (chars.length < min) {
                min = chars.length;
            }
            charsL.add(chars);
        }
        for (int i = 0; i < min; i++) {
            char current = charsL.get(0)[i];
            for (char[] chars : charsL) {
                if (current != chars[i]) {
                    return result.toString();
                }
            }
            result.append(current);
        }
        return result.toString();
    }

//    public String (String[] strs) {
//
//    }
}
