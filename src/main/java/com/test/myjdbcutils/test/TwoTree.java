package com.test.myjdbcutils.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @email bin.peng01@hand-china.com
 * @author 28378
 * @date 2021/10/4 16:51
 */
public class TwoTree {
    private TreeNode rootNode;
    private LinkedBlockingQueue<TreeNode> queue = new LinkedBlockingQueue<>();

    public TwoTree(String data){
        this.rootNode = new TreeNode();
        rootNode.setData(data);
    }
    public void addNode(String data) throws InterruptedException {
        TreeNode treeNode = this.searchAndInstanceNode(rootNode);
        treeNode.setData(data);
    }

    /**
     * 广度遍历
     * @param parentNode 父节点
     * @return 新节点
     */
    private TreeNode searchAndInstanceNode(TreeNode parentNode) throws InterruptedException {
        queue.put(parentNode);
        while(!queue.isEmpty()){
            TreeNode poll = queue.poll();
            if(poll.getLeft() != null){
                queue.put(poll.getLeft());
            }else{
                TreeNode treeNode = new TreeNode();
                poll.setLeft(treeNode);
                return treeNode;
            }
            if(poll.getRight() != null){
                queue.put(poll.getRight());
            }else{
                TreeNode treeNode = new TreeNode();
                poll.setRight(treeNode);
                return treeNode;
            }
        }
        return parentNode;
    }

    public List<TreeNode> leftSearch(){
        ArrayList<TreeNode> treeNodes = new ArrayList<>();
        this.searchOneLeft(treeNodes,this.rootNode);
        return treeNodes;
    }

    public List<TreeNode> centerSearch(){
        ArrayList<TreeNode> treeNodes = new ArrayList<>();
        this.searchOneCenter(treeNodes,this.rootNode);
        return treeNodes;
    }

    private void searchOneLeft(List<TreeNode> list,TreeNode treeNode){
        //左
        if(treeNode.getLeft() != null){
            this.searchOneLeft(list,treeNode.getLeft());
        }

        //中
        list.add(treeNode);

        //右
        if(treeNode.getRight() != null){
            this.searchOneLeft(list,treeNode.getRight());
        }
    }

    private void searchOneCenter(List<TreeNode> list,TreeNode treeNode){
        //中
        list.add(treeNode);

        //左
        if(treeNode.getLeft() != null){
            this.searchOneCenter(list,treeNode.getLeft());
        }

        //右
        if(treeNode.getRight() != null){
            this.searchOneCenter(list,treeNode.getRight());
        }
    }

}
