package com.test.myjdbcutils.test;

import com.test.myjdbcutils.myannotations.MyColumnName;
import com.test.myjdbcutils.myannotations.MyEntity;
import com.test.myjdbcutils.myannotations.MyExcludeColumn;
import com.test.myjdbcutils.myannotations.MyTableName;

/**
 * @author pb
 */
@MyTableName(table = "api_security")
@MyEntity("1")
public class TestEntity {
    private Integer id;
    @MyColumnName("path")
    private String path;
    @MyExcludeColumn
    private String authorities;
    private String name;
    private String label;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    @Override
    public String toString() {
        return "TestEntity{" +
                "id='" + id + '\'' +
                ", path='" + path + '\'' +
                ", authorities='" + authorities + '\'' +
                '}';
    }

    public String getThreadLocalValue(ThreadLocal threadLocal){
        return String.valueOf(threadLocal.get());
    }

    public String getThreadLocalValue2(){
        return new ThreadLocal<String>().get();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
