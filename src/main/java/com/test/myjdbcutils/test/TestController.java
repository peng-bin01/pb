package com.test.myjdbcutils.test;

import com.test.myjdbcutils.myannotations.*;
import com.test.myjdbcutils.springutils.MyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author pb
 */
@MyController
public class TestController {
    @MyAutoWired
    private TestMapper testMapper;
    @MyAutoWired
    private MyFactoryBean myFactoryBean;
    @MyRequestMapping("/test")
    @AopTestAnnotation
    public TestEntity test(@MyRequestParam("test") String[] test, TestEntity testEntity){
        return this.testMapper.test1(1,"wqweqw");
    }


    @MyRequestMapping("/test2")
    public void test2(TestEntity testEntity){
        System.out.println(testEntity);
    }
}
