package com.test.myjdbcutils.test;

import com.test.myjdbcutils.Service.TestService;
import com.test.myjdbcutils.myannotations.MyBean;
import com.test.myjdbcutils.myannotations.MyConfiguration;
import com.test.myjdbcutils.springutils.MyFactoryBean;

import java.text.SimpleDateFormat;

/**
 * @author pb
 */
@MyConfiguration
public class TestConfiguration {

    @MyBean
    public TestService test(MyFactoryBean myFactoryBean,SimpleDateFormat si) {
        System.out.println(myFactoryBean);
        return new TestService();
    }

    @MyBean
    public MyFactoryBean test2(TestMapper testMapper) {
        System.out.println(testMapper.selectAll());
        return new MyFactoryBean();
    }


}
