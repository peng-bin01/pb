package com.test.myjdbcutils.utils;

import com.test.myjdbcutils.basemapper.BaseMapper;
import com.test.myjdbcutils.entity.MethodInfo;
import com.test.myjdbcutils.myannotations.*;
import com.test.myjdbcutils.test.TestEntity;
import org.springframework.util.StringUtils;

import java.lang.reflect.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author pb
 */
public class SqlHandler {
    //通过反射得到注解中的sql
    public static Map<String, MethodInfo> getSql(List<Class> allClass) {
        Map<String, MethodInfo> sqlS = new HashMap<>();
        //           读取Bean信息和上面的注解内容
        for (Class<?> jdbcOpe : allClass) {
            Method[] methods = jdbcOpe.getDeclaredMethods();
            for (Method method : methods) {
                MyJDBCAnnotation annotation = method.getAnnotation(MyJDBCAnnotation.class);
                if (annotation != null) {
                    MethodInfo methodInfo = new MethodInfo();
                    methodInfo.setSql(annotation.sql());
                    Type genericReturnType = method.getGenericReturnType();
                    if (genericReturnType instanceof ParameterizedType) {
                        Type[] actualTypeArguments = ((ParameterizedType) genericReturnType).getActualTypeArguments();
                        Type actualTypeArgument = actualTypeArguments[0];
                        methodInfo.setReturnType((Class<?>) actualTypeArgument);
                    } else {
                        methodInfo.setReturnType((Class<?>) genericReturnType);
                    }
                    sqlS.put(method.getName(), methodInfo);
                }
            }
        }
        return sqlS;
    }

    //    把select开头的sql中的#{}和${}顺序替换成为args里面的参数
    public static MethodInfo sqlPackage(String sql, Method method, Object... args) throws IllegalAccessException {
        String regex = "#\\{[A-Za-z0-9]+\\}|\\$\\{[A-Za-z0-9]+\\}";
        String reSql = "";
        if (StringUtils.isEmpty(sql)) {
            return null;
        }
        Map<String, Object> argMap = getArgMap(method, args);
        Object[] objects = new Object[args.length];
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(sql);
        String[] split = pattern.split(sql);
        //参数位置
        int index = 0;
        //预编译参数位置
        int argIndex = 0;
        while (matcher.find()) {
            String group = matcher.group();
            String fieldName = group.substring(2, group.length() - 1);
            Object o = argMap.get(fieldName);
            if(group.startsWith("#")){
                reSql = reSql.concat(split[index]).concat("?");
                objects[argIndex] = o;
                argIndex++;
            }else if(group.startsWith("$")){
                reSql = reSql.concat(split[index]).concat(String.valueOf(o));
            }
            index++;
        }
        MethodInfo methodInfo = new MethodInfo();
        methodInfo.setSql(reSql);
        methodInfo.setArgsList(Arrays.copyOf(objects, argIndex));
        return methodInfo;
    }

    private static Map<String, Object> getArgMap(Method method, Object[] args) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        Parameter[] parameters = method.getParameters();
        List<Class> originalType = new ArrayList<>();
        originalType.add(String.class);
        originalType.add(int.class);
        originalType.add(Integer.class);
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            //基础数据类型
            if (originalType.contains(arg.getClass())) {
                MyMapperParam annotation = parameters[i].getAnnotation(MyMapperParam.class);
                if (annotation != null) {
                    map.put(annotation.value(), arg);
                } else {
                    map.put(String.valueOf(i), arg);
                }
            } else {
                Field[] declaredFields = arg.getClass().getDeclaredFields();
                for (Field declaredField : declaredFields) {
                    declaredField.setAccessible(true);
                    Object o = declaredField.get(arg);
                    map.put(declaredField.getName(), o);
                }
            }
        }
        return map;
    }

    //    获取基本mapper中的方法和sql
    public static Map<String, MethodInfo> getBaseSql(List<Class> allClass) {
        List<String> fieldList = new ArrayList<>();
        Map<String, MethodInfo> sqlS = new HashMap<>();
        //           读取Bean信息和上面的注解内容
        for (Class jdbcOpe : allClass) {
            Class superclass = jdbcOpe.getSuperclass();
            if (BaseMapper.class.equals(superclass)) {
//                    获取父类的泛型类型
                Type genericSuperclass = jdbcOpe.getGenericSuperclass();
                ParameterizedType pm = (ParameterizedType) genericSuperclass;
//                    获取泛型的实际类型
                Class actualTypeArguments = (Class) pm.getActualTypeArguments()[0];
                MyTableName annotation = (MyTableName) actualTypeArguments.getAnnotation(MyTableName.class);
                String table = annotation.table();
//                获取所有字段 getFields只获取public字段
                Field[] fields = actualTypeArguments.getDeclaredFields();
                for (Field field : fields) {
                    field.setAccessible(true);
                    MyExcludeColumn excludeColumn = field.getAnnotation(MyExcludeColumn.class);
                    MyColumnName columnName = field.getAnnotation(MyColumnName.class);
                    if (excludeColumn == null) {
                        if (columnName != null) {
                            String value = columnName.value();
                            if (!StringUtils.isEmpty(value)) {
                                fieldList.add(value);
                            }
                        } else {
                            String name = field.getName();
                            fieldList.add(name);
                        }
                    }
                }
                String fieldString = fieldList.stream().reduce((var1, var2) -> var1.concat(",").concat(var2)).orElse("*");
                String sql = "select " + fieldString + " from " + table;
                MethodInfo methodInfo = new MethodInfo();
                methodInfo.setSql(sql);
                methodInfo.setReturnType(actualTypeArguments);
                sqlS.put("selectAll", methodInfo);
            }
        }
        return sqlS;
    }

    //    query方法返回数据封装
    public static List<Object> dataPackage(ResultSet resultSet, Class returnClass) throws SQLException, IllegalAccessException, InstantiationException, NoSuchFieldException, ClassNotFoundException {
        List<Object> returnS = new ArrayList<>();
        while (resultSet.next()) {
            ResultSetMetaData metaData = resultSet.getMetaData();
            Object o1 = returnClass.newInstance();
            Field[] fields = returnClass.getDeclaredFields();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String columnName = metaData.getColumnName(i);
                for (Field field : fields) {
                    if (columnName.equals(field.getName())) {
                        field.setAccessible(true);
                        field.set(o1, resultSet.getObject(i));
                    }
                }
            }
            returnS.add(o1);
        }
        return returnS;
    }
}
