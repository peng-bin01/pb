package com.test.myjdbcutils.utils;

import com.test.myjdbcutils.entity.BeanContainer;
import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author pb
 */
public class PackageClassUtils {
    private List<Class> allClass = new ArrayList<>();
    public void getAllPackage(Class c) throws IOException, ClassNotFoundException {
        URL resource = c.getResource("");
        String path = resource.getPath();
        File file = new File(path);
        String packageName = c.getPackage().getName();
        if("file".equals(resource.getProtocol())){
            this.doSelectFile(file, packageName,true);
        }else{
            this.doSelectJar(c);

        }
        BeanContainer.getInstance().setClassList(this.allClass);
    }

    private void doSelectFile(File file, String packageName, boolean isFirst) throws IOException, ClassNotFoundException {
        if(file.isFile()){
            String concat = packageName.concat(".").concat(file.getName());
            Class<?> aClass = Class.forName(concat.substring(0, concat.length() - 6));
            this.allClass.add(aClass);
        }else if(file.isDirectory()){
            if(!isFirst){
                packageName = packageName.concat(".").concat(file.getName());
            }
            File[] files = file.listFiles();
            assert files != null;
            for (File file1 : files) {
                this.doSelectFile(file1,packageName,false);
            }
        }
    }

    private void doSelectJar(Class c) throws IOException, ClassNotFoundException {
        //这句话取到的路径是 服务器 中的发布版本的路径。 jar中使用获取到的是jar包绝对路径
        String resource = c.getProtectionDomain().getCodeSource().getLocation().getFile();
        System.out.println(resource);
        JarFile jarFile = new JarFile(resource);
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry jarEntry = entries.nextElement();
            String name = jarEntry.getName();
            if (name.endsWith(".class") && !name.contains("META-INF")) {
                String replace = null;
                try {
                    replace = name.replace("/", ".").substring(0,name.length()-6);
                    if(replace.startsWith(c.getPackage().getName())){
                        Class<?> aClass = Class.forName(replace);
                        this.allClass.add(aClass);
                    }
                } catch (Error e) {
                    LogManager.getLogger(this.getClass()).error("当前类对象不存在：{}",replace);
                }
            }
        }
    }
}
