package com.test.myjdbcutils.utils;

import com.alibaba.fastjson.JSONObject;
import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.entity.ContentTypes;
import com.test.myjdbcutils.myannotations.MyRequestParam;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author pb
 */
public class ControllerManagerServlet extends GenericServlet {
    //路径后参数
    private Map<String, Object> paraMap = new HashMap<>();
    //方法路径映射表
    private Map<String, Method> methodStringMap;
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        Object returnObject = null;
        byte[] bytes = new byte[1024];
        StringBuilder requestBody = new StringBuilder();
        HttpServletResponse response;
        HttpServletRequest request;
        try {
            response = (HttpServletResponse) servletResponse;
            request = (HttpServletRequest) servletRequest;
        } catch (ClassCastException var6) {
            throw new ServletException("non-HTTP request or response");
        }
        ServletInputStream inputStream = request.getInputStream();
        while (inputStream.read(bytes) != -1) {
            requestBody.append(new String(bytes));
        }
        if("/favicon.ico".equals(request.getPathInfo())){
            InputStream resourceAsStream = this.getClass().getResourceAsStream("/favicon.ico");
            servletResponse.setContentType("image/x-icon");
            ServletOutputStream outputStream = servletResponse.getOutputStream();
            while (resourceAsStream.read(bytes) != -1) {
                outputStream.write(bytes);
            }
            return;
        }
        //参数处理
        this.paraHandler(request,requestBody.toString());
        Method method = this.matchMethodByPath(request.getPathInfo());
        if(method == null){
            response.getWriter().append("请求无对应资源！");
            response.setContentType("application/json;charset=utf-8");
            return;
        }
        Parameter[] parameters = method.getParameters();
        Object[] paraObjects = new Object[parameters.length];
        int index = 0;
        for (Parameter parameter : parameters) {
            Object object = null;
            if (parameter.getType() != String.class && parameter.getType() != String[].class) {
                object = new JSONObject(paraMap).toJavaObject(parameter.getType());
            }else{
                MyRequestParam annotation = parameter.getAnnotation(MyRequestParam.class);
                object = this.paraMap.get(annotation.value());
                if(object instanceof String[] && ((String[])object).length == 1){
                    object = ((String[])object)[0];
                }
            }
            paraObjects[index] = object;
            index ++ ;
        }
        try {
            returnObject = method.invoke(BeanContainer.getInstance().getBean(method.getDeclaringClass()),paraObjects);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        response.setStatus(200);
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().append(JSONObject.toJSONString(returnObject));
    }

    private void paraHandler(HttpServletRequest request,String requestBody){
        String contentType = request.getContentType();
        if (request.getParameterMap() != null) {
            paraMap.putAll(request.getParameterMap());
        }
        if (ContentTypes.JSON.equals(contentType)) {
            paraMap.putAll(JSONObject.parseObject(requestBody).getInnerMap());
        } else if (ContentTypes.FORM.equals(contentType)) {
            String[] split = requestBody.toString().split("&");
            for (String s : split) {
                String[] split1 = s.split("=");
                if (split1.length == 1) {
                    paraMap.put(split1[0].trim(), null);
                } else {
                    paraMap.put(split1[0].trim(), split1[1].trim());
                }
            }
        }
    }

    private Method matchMethodByPath(String path){
        return this.methodStringMap.get(path);
    }

    public void setMethodPath(Map<String, Method> methodStringMap){
        this.methodStringMap = methodStringMap;
    }
}
