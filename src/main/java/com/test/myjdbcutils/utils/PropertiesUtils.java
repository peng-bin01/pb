package com.test.myjdbcutils.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author pb
 */
public class PropertiesUtils {
    static Properties getProperties() throws IOException {
        Properties properties = new Properties();
        InputStream resourceAsStream = PropertiesUtils.class.getResourceAsStream("/application.properties");
        properties.load(resourceAsStream);
        return properties;
    }
}
