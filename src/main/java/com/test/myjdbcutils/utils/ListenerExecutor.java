package com.test.myjdbcutils.utils;

import com.test.myjdbcutils.entity.BeanInfo;
import com.test.myjdbcutils.operation.Listener;

import java.util.List;
import java.util.Map;

/**
 * @author pb
 */
public class ListenerExecutor {
    private List<Listener> listeners = null;
    public ListenerExecutor(List<Listener> listeners){
        this.listeners = listeners;
        for (int i = 0; i < this.listeners.size(); i++) {
            for (int j = i; j < this.listeners.size(); j++) {
                Listener listener = this.listeners.get(i);
                Listener listener1 = this.listeners.get(j);
                if(listener1.order() < listener.order()){
                    this.listeners.set(i,listener1);
                    this.listeners.set(j,listener);
                }
            }
        }
    }

    public void doPreHandler(Map<String, BeanInfo> beanInfoMap){
        this.listeners.forEach(listener -> {
            try {
                listener.preHandler(beanInfoMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
    public void doHandler(Map<String, BeanInfo> beanInfoMap){
        this.listeners.forEach(listener -> {
            try {
                listener.handler(beanInfoMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
    public void doAfterHandler(Map<String, BeanInfo> beanInfoMap){
        this.listeners.forEach(listener -> {
            try {
                listener.afterHandler(beanInfoMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
