package com.test.myjdbcutils.utils;

import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.entity.BeanInfo;
import com.test.myjdbcutils.myannotations.MyEntity;
import com.test.myjdbcutils.operation.Listener;
import com.test.myjdbcutils.operation.ServletListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.CollectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author pb
 */
public class MyApplication {
    private Map<String, BeanInfo> beanInfoMap = new HashMap<>();
    private BeanContainer beanContainer = BeanContainer.getInstance();
    private List<Class> annotationClassList = null;
    private List<Listener> listeners = new ArrayList<>();
    private Logger logger = LogManager.getLogger(this.getClass());



    public BeanContainer run(Class c) throws Exception {
        logger.info("项目入口：{}",c.getTypeName());
        Properties properties = PropertiesUtils.getProperties();
        Properties properties1 = System.getProperties();
        properties.putAll(properties1);
        HashMap<String, String> configMap = new HashMap<>();
        for (Map.Entry<Object, Object> next : properties.entrySet()) {
            configMap.put(((String)next.getKey()),((String)next.getValue()));
        }
        this.beanContainer.setConfigMap(configMap);
        this.beanContainer.setMain(c);
        PackageClassUtils packageClassUtils = new PackageClassUtils();
        packageClassUtils.getAllPackage(c);
        List<Class> classList = beanContainer.getClassList();
        this.classAnalyse(classList);
        return this.beanContainer;
    }

    private void classAnalyse(List<Class> list) throws Exception {
        if (CollectionUtils.isEmpty(list)) {
            logger.info("无任何类注入容器");
            return;
        }
        list = this.filterClass(list);
        beanContainer.setClassList(list);
        for (Class c : list) {
            BeanInfo beanInfo = new BeanInfo();
            beanInfo.setaClass(c);
            this.annotationAnalyse(c, beanInfo);
            this.getListener(c);
            this.pClassAnalyse(c, beanInfo);
            this.beanInfoMap.put(c.getTypeName(), beanInfo);
        }
        ArrayList<BeanInfo> beanInfoList = new ArrayList<>(beanInfoMap.values());
        this.beanContainer.setBeanInfos(beanInfoList);
        ListenerExecutor listenerExecutor = new ListenerExecutor(this.listeners);
        //    整理autowired的别名数据
        listenerExecutor.doPreHandler(beanInfoMap);
        listenerExecutor.doHandler(beanInfoMap);
        listenerExecutor.doAfterHandler(beanInfoMap);
    }

    private void getListener(Class c) throws IllegalAccessException, InstantiationException {
        Class[] anInterface = c.getInterfaces();
        if (anInterface.length != 0) {
            if("com.test.myjdbcutils.operation.Listener".equals(anInterface[0].getTypeName())){
                if(c.equals(ServletListener.class) && "N".equals(this.beanContainer.getEnvConfig("servlet.enable"))){
                    return;
                }
                this.listeners.add((Listener) c.newInstance());
            }
        }
    }

    private List<Class> annotationFilter(List<Class> list) {
        List<Class> annotations = new ArrayList<>();
        annotations.add(MyEntity.class);
        for (Class aClass : list) {
            if (aClass.isAnnotation() && aClass.getAnnotation(MyEntity.class) != null) {
                annotations.add(aClass);
            }
        }
        return annotations;
    }


    private void annotationAnalyse(Class c, BeanInfo beanInfo) {
        Annotation[] annotations1 = c.getAnnotations();
        List<Annotation> annotations2 = Arrays.asList(annotations1);
        beanInfo.setAnnotations(annotations2);
        MyEntity entity = (MyEntity) c.getAnnotation(MyEntity.class);
        if (entity != null) {
            beanInfo.setBeanName(entity.value());
        }
        HashMap<String, List<Annotation>> fieldListHashMap = new HashMap<>();
        HashMap<Method, List<Annotation>> methodListHashMap = new HashMap<>();
        Field[] declaredFields = c.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            Annotation[] annotations = declaredField.getAnnotations();
            List<Annotation> annotationInfos = Arrays.asList(annotations);
            if (annotations.length != 0) {
                fieldListHashMap.put(declaredField.getName(), annotationInfos);
            }
        }
        beanInfo.setFieldAnnotations(fieldListHashMap);

        Method[] declaredMethods = c.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            Annotation[] annotations = declaredMethod.getAnnotations();
            List<Annotation> annotationInfos = Arrays.asList(annotations);
            if(!CollectionUtils.isEmpty(annotationInfos)){
                methodListHashMap.put(declaredMethod, annotationInfos);
            }
        }
        beanInfo.setMethodAnnotations(methodListHashMap);

    }

    private void pClassAnalyse(Class c, BeanInfo beanInfo) {

    }

    private List<Class> filterClass(List<Class> list) {
        this.annotationClassList = this.annotationFilter(list);
        return list.stream().filter(c -> {
            for (Class annotation : this.annotationClassList) {
                if (c.getAnnotation(annotation) != null && !c.isAnnotation()) {
                    return true;
                }
            }
            return false;
        }).collect(Collectors.toList());
    }

//    public static void tomcatStart() throws LifecycleException {
//        Tomcat tomcat = new Tomcat();
//        tomcat.setPort(8085);
//        //相对路径 当前项目地址根目录 user.dir
//        tomcat.setBaseDir("tmp/tomcat");
//        String contextPath = "";
//        StandardContext context = new StandardContext();
//        context.setPath(contextPath);
//        context.addLifecycleListener(new Tomcat.FixContextListener());
//        tomcat.getHost().addChild(context);
//        tomcat.addServlet(contextPath, "controllerManager", new ControllerManagerServlet());
//        context.addServletMappingDecoded("/*", "controllerManager");
//        tomcat.start();
//        tomcat.getServer().await();
//    }

//    private Method getFieldSetMethod(Class aClass,String fieldName , Class fieldClass) throws NoSuchMethodException, NoSuchFieldException {
//        String fieldClassName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
//        String methodName = "set" + fieldClassName;
//        Field[] declaredFields = aClass.getDeclaredFields();
//        Class<?> param = aClass.getDeclaredField(fieldName).getType();
//        System.out.println(methodName);
//        return aClass.getDeclaredMethod(methodName,param);
//    }
}
