package com.test.myjdbcutils.myannotations;


import java.lang.annotation.*;

/**
 * @author pb
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MyAutoWired {
    String value() default "";
}
