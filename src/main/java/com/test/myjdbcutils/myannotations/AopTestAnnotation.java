package com.test.myjdbcutils.myannotations;


import java.lang.annotation.*;

/**
 * @author pb
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AopTestAnnotation {
}
