package com.test.myjdbcutils.myannotations;

import java.lang.annotation.*;

/**
 * @author pb
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MyAopClassAnnotation {
}
