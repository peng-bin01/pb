package com.test.myjdbcutils.myannotations;

import java.lang.annotation.*;

/**
 * @author pb
 */

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MyRequestParam {
    String value() default "";
}
