package com.test.myjdbcutils.myannotations;

import com.test.myjdbcutils.test.TestEntity;

import java.lang.annotation.*;

/**
 * @author pb
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MyAopAnnotation {
    String value() default "";
    Class classValue() default TestEntity.class;
}
