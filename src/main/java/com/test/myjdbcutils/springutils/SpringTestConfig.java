package com.test.myjdbcutils.springutils;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.io.ClassPathResource;

/**
 * @author pb
 */
@Configuration
@MyPackageAnnotation
@ComponentScan("com.test.myjdbcutils")
//@EnableAspectJAutoProxy(proxyTargetClass = true)
public class SpringTestConfig {
}
