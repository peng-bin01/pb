package com.test.myjdbcutils.springutils;

import com.test.myjdbcutils.test.TestAspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @email bin.peng01@hand-china.com
 * @author 28378
 * @date 2021/8/31 8:58
 */
@Service
public class SpringContextUtils implements ApplicationContextAware {
    private static Map<Class, Object> objectMap = new HashMap<Class, Object>();
    private static Map<String,String> environmentMap = new HashMap<>();


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        String property = applicationContext.getEnvironment().getProperty("servlet.enable");
        TestAspect bean = applicationContext.getBean(TestAspect.class);
        objectMap.put(TestAspect.class,bean);
        environmentMap.put("servlet.enable",property);

    }

    public static String getProperty(String key){
        return environmentMap.get(key);
    }

    public static <T> T getBean(Class<T> c){
        return (T) objectMap.get(c);
    }


}
