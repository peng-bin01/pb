package com.test.myjdbcutils.springutils;

import com.test.myjdbcutils.cglibhandler.MyMapperInterceptor;
import net.sf.cglib.proxy.Enhancer;
import org.springframework.beans.factory.FactoryBean;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author pb
 */
public class MyFactoryBean implements FactoryBean {
    public Class mapperClass;
    private MyMapperInterceptor myMapperInterceptor = null;

    public void setMyMapperInterceptor(MyMapperInterceptor myMapperInterceptor) {
        this.myMapperInterceptor = myMapperInterceptor;
    }

    public void setMapperClass(Class mapperClass) {
        this.mapperClass = mapperClass;
    }

    @Override
    public Object getObject() throws PropertyVetoException, SQLException, IOException, ClassNotFoundException {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(mapperClass);
        enhancer.setCallback(myMapperInterceptor);
        return enhancer.create();
    }

    @Override
    public Class<?> getObjectType() {
        return mapperClass;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
