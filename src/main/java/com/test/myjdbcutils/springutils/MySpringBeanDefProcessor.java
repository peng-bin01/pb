package com.test.myjdbcutils.springutils;

import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.entity.BeanInfo;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

import java.util.List;

/**
 * @author pb
 */
public class MySpringBeanDefProcessor implements BeanDefinitionRegistryPostProcessor {
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
//        MyMapperInterceptor myMapperInterceptor = new MyMapperInterceptor();
//        List<Class> allClass = null;
//        try {
//            allClass = new PackageAnnotationHandler().getAllClass();
//            Map<String, MethodInfo> sqlS = SqlHandler.getSql(allClass);
//            Map<String, MethodInfo> baseSql = SqlHandler.getBaseSql(allClass);
//            sqlS.putAll(baseSql);
//            myMapperInterceptor.setSqlS(sqlS);
//        } catch (IOException | ClassNotFoundException | NoSuchMethodException | SQLException | PropertyVetoException e) {
//            e.printStackTrace();
//        }
//        assert allClass != null;
//        for (Class<?> aClass : allClass) {
//            BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(MyFactoryBean.class);
//            AbstractBeanDefinition mapperClass = beanDefinitionBuilder.addPropertyValue("mapperClass", aClass).addPropertyValue("myMapperInterceptor",myMapperInterceptor).getBeanDefinition();
//            beanDefinitionRegistry.registerBeanDefinition(aClass.getSimpleName(),mapperClass);
//            System.out.println("注入bean："+aClass.getSimpleName());
//        }
        List<BeanInfo> beanInfos = BeanContainer.getInstance().getBeanInfos();
        for (BeanInfo beanInfo : beanInfos) {
            Object bean = beanInfo.getBean();
            if(beanInfo.isBeAspectFlag()){
                bean = beanInfo.getAspectBean();
            }
            BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(AllClassFactoryBean.class);
            AbstractBeanDefinition myClass = beanDefinitionBuilder.addPropertyValue("object", bean).getBeanDefinition();
            if(beanInfo.isBeAspectFlag()){
                beanDefinitionRegistry.registerBeanDefinition(bean.getClass().getSuperclass().getTypeName(),myClass);
            }else{
                beanDefinitionRegistry.registerBeanDefinition(bean.getClass().getTypeName(),myClass);
            }

            System.out.println("注入bean："+bean.getClass().getTypeName());
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }
}
