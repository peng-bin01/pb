package com.test.myjdbcutils.springutils;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author pb
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(MySpringBeanDefProcessor.class)
public @interface MyPackageAnnotation {
    String Package() default " ";
}
