package com.test.myjdbcutils.springutils;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author pb
 */
public class AllClassFactoryBean implements FactoryBean {
    private Object object;
    private Class c;
    public void setObject(Object o){
        this.object = o;
        this.c = object.getClass();
    }
    @Override
    public Object getObject() throws Exception {
        return this.object;
    }

    @Override
    public Class<?> getObjectType() {
        return c;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
