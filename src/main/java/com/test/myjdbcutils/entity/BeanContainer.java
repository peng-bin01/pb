package com.test.myjdbcutils.entity;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author pb
 */
public class BeanContainer {
    private static BeanContainer entityInfo = new BeanContainer();
    private List<Class> classList;
    private List<BeanInfo> beanInfos;
    private List<Object> beanList;
    private Map<String,Class> alisMap;
    private Class main;
    private Map<String,String> configMap;
    private BeanContainer(){};
    public static BeanContainer getInstance(){
        return entityInfo;
    }

    public List<Class> getClassList(){
        return this.classList;
    }

    public void setClassList(List<Class> classList) {
        this.classList = classList;
    }

    public List<BeanInfo> getBeanInfos() {
        return beanInfos;
    }

    public void setBeanInfos(List<BeanInfo> beanInfos) {
        this.beanInfos = beanInfos;
    }

    public List<Object> getBeanList() {
        return beanList;
    }

    public void setBeanList(List<Object> beanList) {
        this.beanList = beanList;
    }

    public Map<String, Class> getAlisMap() {
        return alisMap;
    }

    public void setAlisMap(Map<String, Class> alisMap) {
        this.alisMap = alisMap;
    }

    public Class getMain() {
        return main;
    }

    public void setMain(Class main) {
        this.main = main;
    }

    public <T> T getBean(Class<T> c){
        for (BeanInfo beanInfo : this.beanInfos) {
            if(c.equals(beanInfo.getaClass())){
                if(beanInfo.isBeAspectFlag()){
                    return (T) beanInfo.getAspectBean();
                }else{
                    return (T) beanInfo.getBean();
                }
            }
        }
        return null;
    }

    public <T> T getOriginalBean(Class<T> c){
        for (Object o : this.beanList) {
            if(c.equals(o.getClass())){
                return (T) o;
            }
        }
        return null;
    }

    public Object getBean(String s){
        for (Object o : this.beanList) {
            if(s.equals(o.getClass().getTypeName())){
                return o;
            }
        }
        return null;
    }

    public Map<String, String> getConfigMap() {
        return configMap;
    }

    public void setConfigMap(Map<String, String> configMap) {
        this.configMap = configMap;
    }

    public List<BeanInfo> getBeanInfoListByAnnotation(Class c){
        return this.beanInfos.stream().filter(var1 -> {
            List<Annotation> annotations = var1.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(c)) {
                    return true;
                }
            }
            return false;
        }).collect(Collectors.toList());
    }

    public String getEnvConfig(String key){
        return this.configMap.get(key);
    }

    public static void clear(){
        entityInfo.getAlisMap().clear();
        entityInfo.getClassList().clear();

    }
}
