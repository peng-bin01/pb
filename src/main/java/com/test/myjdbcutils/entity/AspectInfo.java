package com.test.myjdbcutils.entity;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author pb
 */
public class AspectInfo {
    //被代理的类
    private Class beAspectClass;
    //被代理的方法和对应的切面方法
    private Map<Method,Method> methodMethodMap;

    public Class getBeAspectClass() {
        return beAspectClass;
    }

    public void setBeAspectClass(Class beAspectClass) {
        this.beAspectClass = beAspectClass;
    }

    public Map<Method, Method> getMethodMethodMap() {
        return methodMethodMap;
    }

    public void setMethodMethodMap(Map<Method, Method> methodMethodMap) {
        this.methodMethodMap = methodMethodMap;
    }
}
