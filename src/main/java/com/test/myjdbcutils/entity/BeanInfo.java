package com.test.myjdbcutils.entity;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author pb
 */
public class BeanInfo {
    private Class aClass;
    private String beanName;
    private Map<String, List<Annotation>> fieldAnnotations;
    private Map<Method, List<Annotation>> methodAnnotations;
    private Object bean;
    private boolean isBeAspectFlag = false;
    private Object aspectBean;
    private List<Annotation> Annotations;

    public Class getaClass() {
        return aClass;
    }

    public void setaClass(Class aClass) {
        this.aClass = aClass;
    }

    public Map<String, List<Annotation>> getFieldAnnotations() {
        return fieldAnnotations;
    }

    public void setFieldAnnotations(Map<String, List<Annotation>> fieldAnnotations) {
        this.fieldAnnotations = fieldAnnotations;
    }

    public Map<Method, List<Annotation>> getMethodAnnotations() {
        return methodAnnotations;
    }

    public void setMethodAnnotations(Map<Method, List<Annotation>> methodAnnotations) {
        this.methodAnnotations = methodAnnotations;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public Object getBean() {
        return bean;
    }

    public void setBeAspectFlag(boolean flag, Object o) {
        this.isBeAspectFlag = flag;
        this.aspectBean = o;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public Map.Entry<Method, List<Annotation>> getMethodByAnnotation(Class annotation) {
        Iterator<Map.Entry<Method, List<Annotation>>> iterator = this.methodAnnotations.entrySet().iterator();
        if (iterator.hasNext()) {
            Map.Entry<Method, List<Annotation>> next = iterator.next();
            for (Annotation annotation1 : next.getValue()) {
                if (annotation1.annotationType().equals(annotation)) {
                    return next;
                }
            }
        }
        return null;
    }

    public boolean isBeAspectFlag() {
        return isBeAspectFlag;
    }

    public Object getAspectBean() {
        return aspectBean;
    }

    public List<Annotation> getAnnotations() {
        return Annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        Annotations = annotations;
    }

}
