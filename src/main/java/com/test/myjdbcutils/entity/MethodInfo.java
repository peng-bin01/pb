package com.test.myjdbcutils.entity;

/**
 * @author pb
 */
public class MethodInfo {
    private String sql;
    private Class<?> returnType;
    private Object[] argsList;

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Class<?> getReturnType() {
        return returnType;
    }

    public void setReturnType(Class<?> returnType) {
        this.returnType = returnType;
    }

    public Object[] getArgsList() {
        return argsList;
    }

    public void setArgsList(Object[] argsList) {
        this.argsList = argsList;
    }
}
