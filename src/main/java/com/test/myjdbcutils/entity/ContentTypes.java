package com.test.myjdbcutils.entity;

/**
 * @author pb
 */
public class ContentTypes {
    public static final String FORM = "application/x-www-form-urlencoded";
    public static final String JSON = "application/json";
}
