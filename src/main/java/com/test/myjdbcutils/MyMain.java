package com.test.myjdbcutils;

import com.mchange.v2.beans.swing.TestBean;
import com.test.myjdbcutils.entity.BeanContainer;
import com.test.myjdbcutils.springutils.SpringContextUtils;
import com.test.myjdbcutils.springutils.SpringTestConfig;
import com.test.myjdbcutils.test.*;
import com.test.myjdbcutils.utils.MyApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.Map;

/**
 * @author pb
 */
public class MyMain {
    public static void main(String[] args) throws Exception {
//        TestAspect testAspect = new TestAspect();
//        System.out.println(testAspect.isPalindrome(1221));
//        System.out.println(testAspect.intToRoman(3));
//        System.out.println(System.getProperty("user.dir"));
//        System.out.println(testAspect.longestCommonPrefix(new String[]{"abcdds","ab","abc"}));
////        //自己容器操作
        BeanContainer run = new MyApplication().run(MyMain.class);
//        TestMapper bean = run.getBean(TestMapper.class);

//        AspectUser bean = run.getBean(AspectUser.class);
//        bean.test();
//        bean.test2();

//        spring容器操作
//        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(SpringTestConfig.class);
//        TestEntity bean = annotationConfigApplicationContext.getBean(TestEntity.class);
//        System.out.println(SpringContextUtils.getBean(TestAspect.class));
//        AspectUser bean1 = annotationConfigApplicationContext.getBean(AspectUser.class);
//        Object bean = annotationConfigApplicationContext.getBean(AspectUser.class.getTypeName());
//        bean1.test2();
//        TwoTree twoTree = new TwoTree("1");
//        twoTree.addNode("2");
//        twoTree.addNode("3");
//        twoTree.addNode("4");
//        twoTree.addNode("5");
//        twoTree.addNode("6");
//        twoTree.addNode("7");
//        twoTree.addNode("8");
//        List<TreeNode> treeNodes = twoTree.leftSearch();
//        List<TreeNode> treeNodes2 = twoTree.centerSearch();
//        for (TreeNode node : treeNodes) {
//            System.out.println(node.getData());
//        }
    }
}
