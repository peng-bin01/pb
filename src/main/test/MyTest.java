import com.test.myjdbcutils.test.MyThreadFactory;
import org.junit.Test;

import java.util.concurrent.*;

public class MyTest {
    @Test
    public void test1() throws ExecutionException, InterruptedException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                5, //核心线程数
                10, //最大线程数
                120,//额外线程死亡时间（10-5）
                TimeUnit.SECONDS,//上方时间单位为s
                new LinkedBlockingQueue<>(5),//任务队列
                new MyThreadFactory(),//产生线程的工厂
                new ThreadPoolExecutor.DiscardOldestPolicy()//拒绝策略 抛弃最先进入的线程
        );

        threadPoolExecutor.execute(() -> System.out.println(Thread.currentThread().getName()));

        Future<String> submit = threadPoolExecutor.submit(() -> "nmsl");
        String x = submit.get();
        System.out.println(x);
    }
}
